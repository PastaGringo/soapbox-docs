import { themes as prismThemes } from 'prism-react-renderer';
import type { Config } from '@docusaurus/types';
import type * as Preset from '@docusaurus/preset-classic';

const config: Config = {
  title: 'Soapbox Docs',
  tagline: 'Powering the future of social media.',
  favicon: 'img/favicon.ico',

  url: 'https://docs.soapbox.pub',
  baseUrl: '/',
  trailingSlash: true,

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      {
        docs: {
          id: 'soapbox',
          path: 'soapbox',
          routeBasePath: '/soapbox',
          sidebarPath: './sidebars/soapbox.ts',
          editUrl: 'https://gitlab.com/soapbox-pub/soapbox-docs/-/blob/main/',
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      } satisfies Preset.Options,
    ],
  ],

  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'ditto',
        path: 'ditto',
        routeBasePath: '/ditto',
        sidebarPath: './sidebars.ts',
        editUrl: 'https://gitlab.com/soapbox-pub/soapbox-docs/-/blob/main/',
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'mostr',
        path: 'mostr',
        routeBasePath: '/mostr',
        sidebarPath: './sidebars.ts',
        editUrl: 'https://gitlab.com/soapbox-pub/soapbox-docs/-/blob/main/',
      },
    ],
  ],

  themeConfig: {
    image: 'img/social-card.png',
    navbar: {
      title: 'Docs',
      logo: {
        alt: 'Soapbox',
        src: 'img/soapbox.svg',
      },
      items: [
        { to: '/soapbox', label: 'Soapbox', position: 'left' },
        { to: '/ditto', label: 'Ditto', position: 'left' },
        { to: '/mostr', label: 'Mostr', position: 'left' },
        { href: 'https://soapbox.pub', label: 'soapbox.pub', position: 'right' },
      ],
    },
    footer: {
      style: 'light',
      links: [
        {
          title: 'Product',
          items: [
            { label: 'Blog', to: 'https://soapbox.pub/blog/' },
            { label: 'About', to: 'https://soapbox.pub/about/' },
            { label: 'Releases', to: 'https://soapbox.pub/releases/' },
          ],
        },
        {
          title: 'Resources',
          items: [
            { label: 'Servers', to: 'https://soapbox.pub/servers/' },
            { label: 'Source code', to: 'https://gitlab.com/soapbox-pub' },
            { label: 'Documentation', to: 'https://docs.soapbox.pub/' },
          ],
        },
        {
          title: 'Community',
          items: [
            { label: 'Donate', to: 'https://soapbox.pub/donate/' },
            { label: 'Weblate', to: 'https://hosted.weblate.org/engage/soapbox-pub/' },
            { label: 'Found a bug?', to: 'https://gitlab.com/soapbox-pub/soapbox/-/issues' },
          ],
        },
      ],
      copyright: `♡${new Date().getFullYear()} Soapbox contributors. Please copy and share.`,
    },
    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
    },
  } satisfies Preset.ThemeConfig,
};

export default config;
