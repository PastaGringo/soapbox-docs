import type { SidebarsConfig } from '@docusaurus/plugin-content-docs';

const sidebars: SidebarsConfig = {
  soapbox: [
    'index',
    {
      type: 'category',
      label: 'Install',
      items: [
        'install/pleroma',
        'install/mastodon',
        'install/subdomain',
      ],
    },
    'customization',
    {
      type: 'category',
      label: 'Development',
      items: [
        'development/how-it-works',
        'development/local',
        'development/yarn-commands',
        'development/build-config',
        'development/backend',
      ],
    },
    {
      type: 'category',
      label: 'Administration',
      items: [
        'administration/updating',
        'administration/removing',
        'administration/deploy-at-scale',
      ],
    },
    'contributing',
  ],
};

export default sidebars;
