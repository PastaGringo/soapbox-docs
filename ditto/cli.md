# Useful Commands

This guide will explain useful commands.

## Making a user admin
```sh
deno task admin:role <pubkey-hex-format> admin
```
