# Ditto Architecture

![Ditto Architecture](./img/ditto-architecture.svg)

Ditto is a Deno/TypeScript application comprised of the following parts:

- **Web Server (HTTP)** - web server built with [Hono](https://hono.dev/).
- **Mastodon API** (`/api`) - implementation of Mastodon's client-server [REST API](https://docs.joinmastodon.org/methods/).
- **Nostr Relay** (`/relay`) - [NIP-01](https://github.com/nostr-protocol/nips/blob/master/01.md) relay implementation.
- **SQLite** - database for storing Nostr events.
- **Relay Pool** - connection pool to a list of Nostr relays, configured by the Ditto admin.
- **Firehose** - stream of Nostr events from the Relay Pool to the Pipeline.
- **Pipeline** - processes all Nostr events that go through Ditto, from all sources.
