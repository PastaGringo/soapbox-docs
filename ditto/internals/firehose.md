# Ditto Firehose

A "firehose" is a continuous stream of events. The term was first popularized by Twitter's Firehose API, which provided access to all public tweets in real-time. More recently it has been used by [Bluesky](https://docs.bsky.app/docs/advanced-guides/firehose) to get a stream of all events from the ATProto network.

In Nostr, a firehose can be described as an empty filter:

```json
["REQ", "_", {}]
```

By sending this request to many relays, and keeping the connections open, you can get a stream of a sizeable portion of the network.

Ditto creates a firehose by subscribing to the relays you configure, and then saving or processing the events that flow through it. This allows feeds to stay updated, and enables algorithms such as trending hashtags.

## Firehose Configuration

The [`DITTO_NSEC`](/ditto/nsec) publishes a [NIP-65](https://github.com/nostr-protocol/nips/blob/master/65.md) relay list, which can be configured through the Soapbox dashboard. Relays with "read+write" permission will be used for the firehose.

To disable the firehose, set `DITTO_FIREHOSE=false` in your environment.