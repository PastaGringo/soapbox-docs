# Setting up a reverse proxy

This guide will focus on nginx, but you can set it up with any webserver using
the concepts outlined here.

## Proxy setup

The most basic Ditto + nginx setup simply proxies HTTP and WebSocket connections
to the Ditto instance and sets up nginx to serve static files.

```conf
server {
    # Change this to your domain.
    server_name ditto.example.com;
    client_max_body_size 6M;

    location /packs {
        add_header Cache-Control "public, max-age=31536000, immutable";
        add_header Strict-Transport-Security "max-age=31536000" always;
        root /opt/ditto/public;
    }

    location ~ ^/(instance|sw.js$|sw.js.map$) {
        root /opt/ditto/public;
    }

    location /images {
        root /opt/ditto/static;
    }

    location / {
        # Change the port number here if you've configured 
        # Ditto to listen on a different port.
        proxy_pass http://localhost:8000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
    
    listen 80;
}
```

However, this setup is insecure and listens with plain HTTP. To set up HTTPS,
read on.

## Setting up [Certbot](https://certbot.eff.org/)

Certbot is a free, open source software tool for automatically obtaining,
setting up, and renewing Let’s Encrypt certificates to enable HTTPS.

Install Certbot using the instructions outlined
[here](https://certbot.eff.org/instructions).

Then, simply run:

```sh
certbot --nginx
```

You will be guided through setting up Certbot, and prompted to select the site
you want to use Let's Encrypt certificates with. Pick the URL of your Ditto
instance.

Certbot will automatically edit your site config to use SSL the certificates it
installs, redirect insecure requests, and renew them as necessary.

That's it! Congratulations, you are now the proud owner of your very own Ditto
instance :D
