# Signing on Ditto

![Ditto Signing](./img/ditto-sign.svg)

When users need to sign Nostr events (eg when posting a status), Ditto emits a [NIP-46](https://github.com/nostr-protocol/nips/blob/master/46.md) request on its relay, tagging the user's pubkey.

Connect events can be trusted because they're signed by the Ditto server nsec, which can be found through `GET /api/v1/instance` or a [NIP-11](https://github.com/nostr-protocol/nips/blob/master/11.md) lookup.

## Signing with Soapbox

![Ditto Signing Soapbox](./img/ditto-sign-soapbox.svg)

Soapbox is both a Mastodon App and a Remote Signer, so if you're using Soapbox (and storing keys in the browser), it will sign events for you.
