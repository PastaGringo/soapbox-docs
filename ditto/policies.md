# Ditto Moderation Policies

By placing a TypeScript file at `data/policy.ts`, you can write a custom script that either accepts or rejects events.

Example policy:

```ts
import { NPolicy, NostrEvent, NostrRelayOK } from '@nostrify/nostrify';
import { AntiDuplicationPolicy, KeywordPolicy, PipePolicy } from '@nostrify/nostrify/policies';

const kv = await Deno.openKv();

export default class AppPolicy implements NPolicy {
  async call(event: NostrEvent): Promise<NostrRelayOK> {
    const policy = new PipePolicy([
      new KeywordPolicy(['https://t.me/']),
      new AntiDuplicationPolicy({ kv, minLength: 50 }),
    ]);

    return policy.call(event);
  }
}

```

The default export must contain a `call` function that takes an event and returns a promise with a message like `['OK', event.id, true, '']` to accept the event, or `['REJECT', event.id, false, 'blocked: event did not meet criteria']` to reject it.

Policy classes can be used from [Nostrify](https://nostrify.dev/policy/).