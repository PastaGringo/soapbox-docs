# Soapbox

![Soapbox Overview](./img/soapbox-overview.svg)

Soapbox is a [Mastodon API](https://docs.joinmastodon.org/methods/) client compatible with a variety of backends. It can be run as the main frontend for your server, or as an external client with OAuth.
